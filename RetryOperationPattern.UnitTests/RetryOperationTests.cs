﻿using NUnit.Framework;
using System;
namespace RetryOperationPattern.UnitTests {
	[TestFixture]
	public class RetryOperationTests {
		private readonly TimeSpan SHORT_RETRY = TimeSpan.FromMilliseconds( 100 );

		[Test]
		public void OnRetry_IfSuccessful_OnlyCallOnce( ) {
			int timesCalled = 0;
			IRetryOperation retryOperation = new RetryOperation( );
			bool success = retryOperation.Retry( ( ) => {
				timesCalled++;
				return true;
			}, new TimeSpan[] { SHORT_RETRY } );
			Assert.IsTrue( success );
			Assert.AreEqual( 1, timesCalled );
		}

		[Test]
		public void OnRetry_IfUnsuccessful_ReturnFalse( ) {
			IRetryOperation retryOperation = new RetryOperation( );
			bool success = retryOperation.Retry( ( ) => {
				return false;
			}, new TimeSpan[] { SHORT_RETRY } );
			Assert.IsFalse( success );
		}

		[Test]
		public void OnRetry_IfUnsuccessful_Retry( ) {
			int timesCalled = 0;
			IRetryOperation retryOperation = new RetryOperation( );
			bool success = retryOperation.Retry( ( ) => {
				timesCalled++;
				return timesCalled == 2;
			}, new TimeSpan[] { SHORT_RETRY } );
			Assert.IsTrue( success );
			Assert.AreEqual( 2, timesCalled );
		}

		[Test]
		public void OnRetry_IfUnsuccessful_RetryUntilLimitReached( ) {
			int timesCalled = 0;
			IRetryOperation retryOperation = new RetryOperation( );
			bool success = retryOperation.Retry( ( ) => {
				timesCalled++;
				return false;
			}, new TimeSpan[] { SHORT_RETRY, SHORT_RETRY } );
			Assert.IsFalse( success );
			Assert.AreEqual( 3, timesCalled );
		}

		[Test]
		public void OnRetry_IfExceptionOccurs_IgnoreUnlessLast( ) {
			int timesCalled = 0;
			IRetryOperation retryOperation = new RetryOperation( );
			Assert.Throws<StackOverflowException>( ( ) => {
				retryOperation.Retry( ( ) => {
					timesCalled++;
					if( timesCalled == 3 ) {
						throw new StackOverflowException( );
					}
					throw new Exception( );
				}, new TimeSpan[] { SHORT_RETRY, SHORT_RETRY } );
			} );
		}
	}
}
