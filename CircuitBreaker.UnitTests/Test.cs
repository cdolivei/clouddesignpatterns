﻿using NUnit.Framework;
using System;
using System.Threading;

namespace CircuitBreaker.UnitTests {
	[TestFixture]
	public class Test {

		[Test]
		public void OnRunOperation_WhenCalled_ActuallyRun( ) {
			bool ran = false;
			ICircuitBreaker breaker = new CircuitBreaker( 3, TimeSpan.FromSeconds( 1 ) );

			Assert.IsTrue( breaker.RunOperation( ( ) => {
				ran = true;
				return true;
			} ) );

			Assert.IsTrue( ran );
		}

		[Test]
		public void OnRunOperation_OnSuccess_ReturnTrue( ) {
			ICircuitBreaker breaker = new CircuitBreaker( 3, TimeSpan.FromSeconds( 1 ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
		}

		[Test]
		public void OnRunOperation_AfterThresholdMet_ReturnFalse( ) {
			ICircuitBreaker breaker = new CircuitBreaker( 3, TimeSpan.FromHours( 1 ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( SuccessRun ) );
		}

		[Test]
		public void OnRunOperation_WhenOpenState_VerifyWeRespectTheTimeout( ) {
			ICircuitBreaker breaker = new CircuitBreaker( 3, TimeSpan.FromSeconds( 1 ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Thread.Sleep( TimeSpan.FromSeconds( 2 ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
		}

		[Test]
		public void OnRunOperation_WhenHalfOpenState_BackToOpenOnFailure( ) {
			ICircuitBreaker breaker = new CircuitBreaker( 1, TimeSpan.FromSeconds( 1 ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Thread.Sleep( TimeSpan.FromSeconds( 2 ) );
			Assert.IsTrue( breaker.RunOperation( SuccessRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( SuccessRun ) );
		}

		[Test]
		public void OnRunOperation_WhenHalfOpenState_GoBackToClosedAfterThreshold( ) {
			ICircuitBreaker breaker = new CircuitBreaker( 2, TimeSpan.FromSeconds( 1 ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) );
			Assert.IsFalse( breaker.RunOperation( FailedRun ) ); // Triggers open state
			Thread.Sleep( TimeSpan.FromSeconds( 2 ) );           // Causes the state to move to Half-Open
			Assert.True( breaker.RunOperation( SuccessRun ) );
			Assert.True( breaker.RunOperation( SuccessRun ) );   // Should now be in the closed state
			Assert.False( breaker.RunOperation( FailedRun ) );
			Assert.True( breaker.RunOperation( SuccessRun ) );
		}

		private bool SuccessRun( ) {
			return true;
		}
		private bool FailedRun( ) {
			return false;
		}
	}
}
