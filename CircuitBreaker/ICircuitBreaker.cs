﻿using System;
namespace CircuitBreaker {
	public interface ICircuitBreaker {
		bool RunOperation( Func<bool> operation );
	}
}
