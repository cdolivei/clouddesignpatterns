﻿using System;
namespace CircuitBreaker.States {
	internal class OpenState : ICircuitBreakerState {
		private CircuitBreaker m_circuitBreaker;
		private readonly DateTime m_timeoutDate;

		public OpenState( CircuitBreaker circuitBreaker, TimeSpan timeoutTimer ) {
			m_circuitBreaker = circuitBreaker;
			m_timeoutDate = DateTime.Now + timeoutTimer;
		}

		bool ICircuitBreakerState.RunOperation( Func<bool> operation ) {
			if( DateTime.Now > m_timeoutDate ) {
				m_circuitBreaker.SetState( StateEnum.HalfOpen );
				return m_circuitBreaker.RunOperation( operation );
			}
			return false;
		}
	}
}
