﻿using System;
namespace CircuitBreaker.States {
	internal class HalfOpen : ICircuitBreakerState {
		private readonly CircuitBreaker m_circuitBreaker;
		private readonly uint m_successThreshold;

		uint m_successCount;

		public HalfOpen( CircuitBreaker circuitBreaker, uint successThreshold  ) {
			m_circuitBreaker = circuitBreaker;
			m_successThreshold = successThreshold;
			m_successCount = 0;
		}

		bool ICircuitBreakerState.RunOperation( Func<bool> operation ) {
			bool success;
			try {
				success = operation.Invoke( );
			} catch {
				success = false;
			}

			if( !success ) {
				m_circuitBreaker.SetState( StateEnum.Open );
			} else {
				m_successCount++;

				if( m_successCount >= m_successThreshold ) {
					m_circuitBreaker.SetState( StateEnum.Closed );
				}
			}
			return success;
		}
	}
}
