﻿using System;
namespace CircuitBreaker.States {
	internal interface ICircuitBreakerState {
		bool RunOperation( Func<bool> operation );
	}
}
