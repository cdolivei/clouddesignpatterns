﻿using System;
namespace CircuitBreaker.States {
	internal class ClosedState : ICircuitBreakerState {
		private readonly CircuitBreaker m_circuitBreaker;
		private readonly uint m_failureThreshold;
		private readonly TimeSpan m_timeoutTimer;

		private uint m_failCount;
		private DateTime m_timePeriodPoint;

		public ClosedState( CircuitBreaker circuitBreaker, uint failureThreshold, TimeSpan timeoutTimer ) {
			m_circuitBreaker = circuitBreaker;
			m_failureThreshold = failureThreshold;
			m_timeoutTimer = timeoutTimer;

			ResetTimePeriod( );
		}

		bool ICircuitBreakerState.RunOperation( Func<bool> operation ) {
			bool success;

			try {
				success = operation.Invoke( );
			} catch {
				success = false;
			}

			if( success ) {
				return success;
			}

			if( DateTime.Now > m_timePeriodPoint ) {
				ResetTimePeriod( );
			}

			if( !success ) {
				m_failCount++;
			}

			if( m_failCount >= m_failureThreshold ) {
				m_circuitBreaker.SetState( StateEnum.Open );
			}

			return false;
		}

		private void ResetTimePeriod( ) {
			m_timePeriodPoint = DateTime.Now + m_timeoutTimer;
			m_failCount = 0;
		}
	}
}
