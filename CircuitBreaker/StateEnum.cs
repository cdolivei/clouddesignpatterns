﻿namespace CircuitBreaker {
	internal enum StateEnum {
		Closed,
		Open,
		HalfOpen
	}
}
