﻿using System;
using CircuitBreaker.States;

namespace CircuitBreaker {
	public class CircuitBreaker : ICircuitBreaker {
		private readonly uint m_failureThreshold;
		private readonly TimeSpan m_timeoutTimer;

		private ICircuitBreakerState m_circuitBreakerState;

		public CircuitBreaker( uint failureThreshold, TimeSpan timeoutTimer ) {
			m_failureThreshold = failureThreshold;
			m_timeoutTimer = timeoutTimer;
			SetState( StateEnum.Closed );
		}

		public bool RunOperation( Func<bool> operation ) {
			return m_circuitBreakerState.RunOperation( operation );
		}

		internal void SetState( StateEnum newState ) {
			switch( newState ) {
				case StateEnum.Open:
					m_circuitBreakerState = new OpenState( this, m_timeoutTimer );
					break;
				case StateEnum.Closed:
					m_circuitBreakerState = new ClosedState( this, m_failureThreshold, m_timeoutTimer );
					break;
				case StateEnum.HalfOpen:
					m_circuitBreakerState = new HalfOpen( this, m_failureThreshold );
					break;
			}
		}
	}
}
