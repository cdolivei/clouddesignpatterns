# Circuit Breaker Pattern #

The design of this solution was heavily influenced by the Circuit Breaker article on [MSDN](https://msdn.microsoft.com/en-us/library/dn589784.aspx)

This pattern is generally used when communicating with a remote system fails and you want to give it a chance to recover. This pattern:

1. passes requests to remote systems as usual
2. if a certain number of requests fail during a period, fail future requests for a period of time
3. after that time elapses, attempt to pass requests to the remote system
4. after a certain number of successful requests, go back to step 1. If any fail, go to step 2

Circuit breaker by itself does not retry failed requests, as it attempts to solve a different problem. By limiting the number of requests to remote systems, it can prevent the remote system from being overwhelmed after an outage.

## Examples ##


```
#!c#
// The circuit breaker will "trip" after 3 failed attempts in 10 minutes. After which it
// will wait 10 minutes before making any more requests
ICircuitBreaker breaker = new CircuitBreaker.CircuitBreaker( 3, TimeSpan.FromMinutes( 10 ) );
// ...
bool success = breaker.RunOperation( ( ) => {
	// delegate returns true if the command was successful or false if not.
	try {
		m_telemetryProvider.Publish( datapoint );
	} catch {
		return false;
	}

	return true;
} );

if( !success ) {
	// either log a message (probably not a good idea in case there is an outage)
	// or add a journal entry to retry operation another time.
	// if the exception is useful, you can capture it inside the delegate
}
```


# Retry pattern #

Retrying an operation is often a low-effort way to recover from failures that is surprisingly effective. I often see it done as;

```
#!c#
bool successful = false;
for (int tries = 1; tries < 3; ++tries) {
	successful = do_some_operation()
	if ( successful ) {
		break
	}
	Thread.Sleep( 10 seconds )
}

if ( !successful ) {
	// Handle
}
```

You should always handle the case where retries are unsuccessful. The main downside here is that your are blocking a thread, which impacts the performance of your application. Amazon also suggests [waiting an increasing amount of time](http://docs.aws.amazon.com/general/latest/gr/api-retries.html) (for example, 1 second then 10 seconds, 1 minute)

The retry implementation in this project allows you to specify the number and time between retries. The implementation also catches exceptions, and will throw an exception if it's the result of the last retry attempt.

## Example ##
```
#!c#

IRetryOperation retryOperation = new RetryOperation( );

// If the operation can throw an exception, you may want to wrap this in a try-catch block
bool success = retryOperation.Retry( ( ) => {
	// do something, returning true if successful
} , new TimeSpan[] { TimeSpan.FromMilliseconds( 100 ), TimeSpan.FromMilliseconds( 200 ) } );
// The first attempt will happen immediately. If unsuccessful, it will try again after 100
// milliseconds, and if that is also unsuccessful, it will try after 200 milliseconds 
```