﻿using System;
namespace RetryOperationPattern {
	public interface IRetryOperation {
		bool Retry( Func<bool> operation, TimeSpan[] timeBetweenRetries );
	}
}
