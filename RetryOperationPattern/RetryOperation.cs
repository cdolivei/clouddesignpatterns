﻿using System;
using System.Threading;

namespace RetryOperationPattern {
	public class RetryOperation : IRetryOperation {
		bool IRetryOperation.Retry( Func<bool> operation, TimeSpan[] timeBetweenRetries ) {
			bool successful = false;
			int attempt = 1;
			int maxAttempts = timeBetweenRetries.Length + 1;

			do {
				try {
					successful = operation( );
					if( successful ) {
						break;
					}
				} catch {
					if( attempt == maxAttempts ) {
						throw;
					}
				}

				if( attempt == maxAttempts ) {
					break;
				}
				Thread.Sleep( timeBetweenRetries[attempt - 1] );
				++attempt;

			} while( !successful );
			return successful;
		}
	}
}